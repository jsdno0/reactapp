<설치방법>
1. visual studio code(VS code) 설치 (구글링)
2. nodejs 설치 (구글링)
3. apptest.zip 압축해제
4. VS code 실행 후 apptest폴더 열기 (VS code 맨위 윈도우 창 - [파일] - [폴더열기])
5. VS code 맨위 윈도우 창 - [터미널] - [새터미널]

if 아예 빈 폴더인 경우: 
6-1. 터미널 명령어 입력: npm install -g create-react-app
6-2. 터미널 명령어 입력: npx create-react-app myfirstreact
6-3. 터미널 명령어 입력: cd myfirstreact
6-4. 터미널 명령어 입력: npm start
if 안에 package.json이 들어 있는 경우 : 
6. 터미널 명령어 입력: npm config set registry http://registry.npmjs.org/ --global

7. 터미널 명령어 입력: npm config set strict-ssl false
8. 터미널 명령어 입력: npm install 
9. 설치가 끝나면 터미널에 명령어 입력: npm install -g json-server
10. 터미널 명령어 입력: cd src/data
11. 터미널 명령어 입력: json-server --watch data.json --port 3001
12. VS code 맨위 윈도우 창 - [터미널] - [새터미널]
13. 터미널 명령어 입력: npm start
14. 재시작할시, 터미널에서 ctrl+c -> y -> npm start 순으로 입력

<테스트방법>
1. 테스트 순서: 조회버튼 -> 삽입버튼 -> 조회버튼 -> 수정버튼-> 조회버튼 -> 삭제버튼 -> 조회버튼 
2. 소스 분석 순서:
1) src\index.js 
2) src\App.js
3) src\Routes.js
4) src\test\index.js
5) src\test\view\TestView.js
6) src\test\container\TestContainer.js
7) src\test\store\TestStore.js
8) src\test\repository\TestRepository.js
9) src\test\model\TestModel.js
