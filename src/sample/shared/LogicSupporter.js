/* eslint-disable */
function funcNameValueList(entity) {
  const updateDataArr = [];

  for (const key in entity ) {
    updateDataArr.push({ name: key, value: entity[key] });
  }

  return { nameValues: updateDataArr };
}

function funcConvertoLov(payload){
  let final = [];

  if(payload!=null){
    let oneObj;
    for(let key in payload){
      oneObj = {value: key, text: payload[key]};
      final.push(oneObj);
    }
    final.unshift({value: null, text: '선택하세요'})
  }

  return final;
}

function funcResetIdx(payload){
  let final;

  if(payload!=null){
    for(let i=0; i<payload.length; i++){
      payload[i].idx = i;
    }
  }

  final = payload;
  return final;
}

function funcReformCrud(payload){

  let final ={};
  final.list = [];
  final.list.push(payload);

  return final;
}

function funcResetSeq(payload){
  let final;
  let count = 1;
  if(payload!=null){
    if(payload.length>0){
      for(let i=0; i<payload.length; i++){
        payload[i].seq = count;
        count++;
      }
    }else{
      payload.seq = count;
    }
  }

  final = payload;
  return final;
}

function funcSearchByKey(payload, condition){

  let final = [];
  let payload1=[];
  let payload2=[];
  if(payload!=null){
    /* or 조건 start*/
    // if(condition!=null){
    //   let trsCd;
    //   if(condition.trsCd!=null&&condition.trsCd!=''){
    //     trsCd = condition.trsCd;
    //   }
    //   if(trsCd!=null){
    //     if(trsCd!=''){
    //       payload1 = payload.filter(one => one.trsCd == trsCd);
    //     }
    //   }
    //   let vendorNum;
    //   if(condition.vendorNum!=null&&condition.vendorNum!=''){
    //     vendorNum = condition.vendorNum;
    //   }
    //   if(vendorNum!=null&&vendorNum!=''){
    //     payload2 = payload.filter(one => one.vendorNum == vendorNum);
    //   }
    //   let r1 =[];
    //   if(payload1!=null){r1 = r1.concat(payload1)};
    //   if(payload2!=null){r1 = r1.concat(payload2)};
    //   let result = new Set(r1);
    //   if(result.size>0){
    //     final = Array.from(result);
    //   }else if(result.size==0){
    //     final = payload;
    //   }
    // }else{
    //   final = payload;
    // }
    /* or 조건 end*/
    /* and 조건 start */
    if(condition!=null){

      let trsCd;
      if(condition.trsCd!=null&&condition.trsCd!=''){
        trsCd = condition.trsCd;
      }
      if(trsCd!=null){
        if(trsCd!=''){
          payload = payload.filter(one => one.trsCd == trsCd);
        }
      }

      let vendorNum;
      if(condition.vendorNum!=null&&condition.vendorNum!=''){
        vendorNum = condition.vendorNum;
      }
      if(vendorNum!=null&&vendorNum!=''){
        payload = payload.filter(one => one.vendorNum == vendorNum);
      }

    }
    final = payload;
    /* and 조건 end */
  }

  return final;
}

export {funcConvertoLov, funcResetIdx, funcReformCrud, funcResetSeq, funcSearchByKey}
