/* eslint-disable */
import React, { Component } from 'react';
// import SampleSearchView from './SampleSearchView';
import SampleTableView from './SampleTableView';
import { observer } from 'mobx-react';
import autobind from 'autobind-decorator';

@autobind
@observer
class SampleView extends Component {
  static menuItems = [];

  render() {

    const {
      samples
      , funcChangeSample
    } = this.props;



    return (

            <SampleTableView
            samples = {samples}
            funcChangeSample = {funcChangeSample}
            />
    );
  }
}

export default SampleView;
