import { sampleStore } from './store/SampleStore';

import SampleContainer from './container/SampleContainer';


export default {
  sampleStore,
  SampleContainer,
};

export {
  sampleStore,
  SampleContainer,
};
