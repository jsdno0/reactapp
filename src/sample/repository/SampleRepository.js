/* eslint-disable */
import axios from 'axios';
import sampleModel from '../model/SampleModel';

class SampleRepository {
  //
  constructor(){
  }

  funcChangeSample(data) {
    let id = data.id;
    
    // return axios.get(`http://localhost:3001/data`)
    //   .then(response => response.data);
    // return axios.post(`http://localhost:3001/data`, data)
    // .then(response => response.data);
    // return axios.put(`http://localhost:3001/data/${id}`, data)
    // .then(response => response.data);
    return axios.delete(`http://localhost:8080/data/${id}`, data)
    .then(response => response.data);
  }

}

export default SampleRepository;
export const sampleRepository = new SampleRepository();
