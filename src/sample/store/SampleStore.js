/* eslint-disable */
import { observable, action, computed, toJS } from 'mobx';
import { sampleRepository } from '../repository/SampleRepository';
import devdb from '../data/devdb';
import { funcConvertoLov, funcReformCrud, funcResetIdx, funcResetSeq, funcSearchByKey } from '../shared/LogicSupporter';
import autobind from 'autobind-decorator';

@autobind
class SampleStore {

  @observable
  _samples;


  constructor() {
  }

  @computed
  get samples() {
    return toJS(this._samples);
  }


  @action
  async clear() {
    this._samples = null;
  }

  @action
  funcChangeSample(data){
    
    sampleRepository.funcChangeSample(data).then(action((rst)=>{}))
    .catch((e)=>{})
  }


}

export default SampleStore;
export const sampleStore = new SampleStore();
