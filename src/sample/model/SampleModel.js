/* eslint-disable */
import { extendObservable } from 'mobx';


export class SampleModel {

  constructor(sample = {}) {
    extendObservable(this, sample);
  }


  static fromApiModels(samples) {
    //
    if (!Array.isArray(samples)) {
      return this.fromApiModel(samples);
      //return [];
    }
    return samples.map(sample => SampleModel.fromApiModel(sample));
  }

  static fromApiModel(sample) {
    return new SampleModel(sample);
  }

  /**
   * Ui Model을 Api Model로 변환
   */
  toApiModel() {
    return this;
  }

  /**
   * NameValue target
   */
  asNameValues() {
    return {
      nameValues: [
        { name: 'itemNo',       value: JSON.stringify(this.itemNo) },
        { name: 'parentNo',     value: JSON.stringify(this.parentNo) },
        { name: 'shapeType',    value: JSON.stringify(this.shapeType) },
        { name: 'stage',        value: JSON.stringify(this.stage) },
        { name: 'cutDateTime',  value: JSON.stringify(this.cutDateTime) },
      ],
    };
  }
}


export default SampleModel;
