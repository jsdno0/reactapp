/* eslint-disable */	
import React, { Component } from 'react';	
import autobind from 'autobind-decorator';	
import { inject, observer } from 'mobx-react';	

	
	
//import { sampleStore } from '..';	
import SampleView from '../view/SampleView';	
import SampleStore, { sampleStore } from '../store/SampleStore';
	
// @inject('sampleStore')	
@observer	
@autobind
class SampleContainer extends Component {	
  //	
  componentDidMount() {	

  }	

	
  funcChangeSample(){	
      
    let testData = {
      "id": "002",
      "name": "test3"
    };
    sampleStore.funcChangeSample(testData);	
  }
	
  render() {	
    //	
    const {	
        samples	
    } = sampleStore;	
    
	
    return (	
	
        <SampleView	
          samples={samples}	
          funcChangeSample = {this.funcChangeSample}	
        />
    );	
  }	
}	
	
export default SampleContainer;	
