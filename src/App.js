import React from 'react';
import { Provider } from 'react-redux';

// import store from './store';
import Routes from './Routes';
// import store from './storeProduct'
import {sampleStore} from './sample';
import {testStore} from './test';

const App = () => (
  <Provider 
  // store={store}
    sampleStore={sampleStore}
    testStore = {testStore}
  >
    <Routes />
  </Provider>
);

export default App;
