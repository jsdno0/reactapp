import React, {Component} from 'react';
import autobind from 'autobind-decorator';
import { inject, observer } from 'mobx-react';

import {testStore} from '../store/TestStore';
import TestView from '../view/TestView';

@observer
@autobind
class TestContainer extends Component{

    componentDidMount(){
        //함수
        testStore.clear();
    }

    funcChangeTest(){
        testStore.funcChangeTest();
    }

    funcInsert(){
        const randomId = Math.floor(Math.random() * 100);
        const data = {
            "id":randomId,
            "name":"test2"
        }
        testStore.funcInsert(data);
    }

    funcRevise(){
        const data = {
            "id":"002",
            "name":"수정된 테스트 이름"
        }
        const id = data.id;
        testStore.funcRevise(id, data);
    }

    funcDel(){
        const id = "002";
        testStore.funcDel(id);
    }

    render(){

        console.log('debug 1');
        console.log(testStore);

        const {
            tests
        } = testStore;

        return(
            <TestView
                funcChangeTest = {this.funcChangeTest}
                funcInsert = {this.funcInsert}
                funcRevise = {this.funcRevise}
                funcDel = {this.funcDel}
                tests = {tests}
            />
        );
    }

}

export default TestContainer;