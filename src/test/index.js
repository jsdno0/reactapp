import {testStore} from './store/TestStore';
import TestContainer from './container/TestContainer';

export default {
    testStore,
    TestContainer
}

export {
    testStore,
    TestContainer
}