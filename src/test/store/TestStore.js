/* eslint-disable */
import { observable, action, computed, toJS } from 'mobx';
import { testRepository } from '../repository/TestRepository';
import autobind from 'autobind-decorator';

@autobind
class TestStore{

    @observable
    _tests;

    constructor(){

    }

    @computed
    get tests(){
        return toJS(this._tests);
    }

    @action
    async clear(){
        this._tests = null;
    }

    @action
    funcChangeTest(){
        testRepository.funcChangeTest()
            .then(action((response)=>{
                console.log(JSON.stringify(response));
                
                this._tests = toJS(response);
                return this._tests;
            }))
            .catch((e)=>{window.alert(e);});
            // (response)=>{response}  == function(response){response}
    }

    @action
    funcInsert(data){
        testRepository.funcInsert(data)
        .then(action((response)=>{response}))
        .catch((e)=>{window.alert(e);}); 
    }

    @action
    funcRevise(id, data){
        testRepository.funcRevise(id, data)
        .then(action((response)=>{response}))
        .catch((e)=>{window.alert(e);}); 
    }

    @action
    funcDel(id){
        testRepository.funcDel(id)
        .then(action((response)=>{response}))
        .catch((e)=>{window.alert(e);}); 
    }

}

export default TestStore;
export const testStore = new TestStore();