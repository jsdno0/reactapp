/* eslint-disable */
import axios from 'axios';
import TestModel from '../model/TestModel'

class TestRepository{
    constructor(){

    }

    funcChangeTest(){
        return axios.get(`http://localhost:3001/data`)
            .then(response=>
                TestModel.fromApiModels(response.data));
    }

    funcInsert(data){
        return axios.post(`http://localhost:3001/data`, data)
        .then(response=>response.data);
    }

    funcRevise(id, data){
        return axios.put(`http://localhost:3001/data/${id}`, data)
        .then(response=>response.data);
    }

    funcDel(id){
        return axios.delete(`http://localhost:3001/data/${id}`)
        .then(response=>response.data);
    }
}
export default TestRepository;
export const testRepository = new TestRepository();