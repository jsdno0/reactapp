/* eslint-disable */
import React, { Component } from 'react';
import { observer } from 'mobx-react';
import autobind from 'autobind-decorator';

// @autobind
@observer
class TestView extends Component {


    render(){
        const {
            funcChangeTest
            , funcInsert
            , funcRevise
            , funcDel
            , tests
        } = this.props;
        return(
            <div>
            <h3>Test입니다.</h3>
            <button onClick={funcChangeTest}>조회버튼</button>
            <button onClick={funcInsert}>삽입버튼</button>
            <button onClick={funcRevise}>수정버튼</button>
            <button onClick={funcDel}>삭제버튼</button>
            <div>
                {
                    Array.isArray(tests)&&tests!=null?tests.map((a,i,v)=>{
                        
                        return(
                            <div>
                                {v[i].id}
                            /
                            {v[i].name}
                        </div>
                        );
                        // return(
                        //     <span>{v[i]}</span>
                        // )
                    }):'no data'
                }
            </div>
            </div>
        );
    }
}
export default TestView;