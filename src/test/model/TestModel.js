/* eslint-disable */
import { extendObservable } from 'mobx';


export class TestModel {

  constructor(test = {}) {
    extendObservable(this, test);
  }


  static fromApiModels(tests) {
    //
    if (!Array.isArray(tests)) {
      return this.fromApiModel(tests);
      //return [];
    }
    return tests.map(test => TestModel.fromApiModel(test));
  }

  static fromApiModel(test) {
    return new TestModel(test);
  }

  /**
   * Ui Model을 Api Model로 변환
   */
  toApiModel() {
    return this;
  }

//   /**
//    * NameValue target
//    */
//   asNameValues() {
//     return {
//       nameValues: [
//         { name: 'itemNo',       value: JSON.stringify(this.itemNo) },
//         { name: 'parentNo',     value: JSON.stringify(this.parentNo) },
//         { name: 'shapeType',    value: JSON.stringify(this.shapeType) },
//         { name: 'stage',        value: JSON.stringify(this.stage) },
//         { name: 'cutDateTime',  value: JSON.stringify(this.cutDateTime) },
//       ],
//     };
//   }
}


export default TestModel;
