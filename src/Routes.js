
import React, { Fragment, Component }  from 'react';
import { BrowserRouter, Switch, Redirect, Route, } from 'react-router-dom';
import {SampleContainer} from './sample';
import { TestContainer } from './test';

const Routes = () => (
  <BrowserRouter basename="/">
    <Switch>
    <Redirect exact from="/" to="/test"/>
      <Route path={`/test`} component={TestContainer} />
    </Switch>
  </BrowserRouter>
);



export default Routes;
